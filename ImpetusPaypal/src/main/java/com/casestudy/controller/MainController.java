package com.casestudy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController
{
  @RequestMapping("/")  
  public String home()
  {
    System.out.println("in main");
    return "index";
  }
  @RequestMapping("/viewUser")  
  public String viewUser()
  {
    System.out.println("in main");
    return "index";
  }
}
