package com.springbootapplication.rest;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Named
@Path("/")
public class UserRestService
{
  private static List<User> customers = new ArrayList<User>();  
  private UserDao userDao = new UserDao();

  static
  {
    User customer1 = new User();
    customer1.setId(1);
    customer1.setName("Customer 1");
    User customer2 = new User();
    customer2.setId(2);
    customer2.setName("Customer 2");
    User customer3 = new User();
    customer3.setId(3);
    customer3.setName("Customer 3");
    User customer4 = new User();
    customer4.setId(4);
    customer4.setName("Customer 4");
    User customer5 = new User();
    customer5.setId(5);
    customer5.setName("Customer 5");
    customers.add(customer1);
    customers.add(customer2);
    customers.add(customer3);
    customers.add(customer4);
    customers.add(customer5);
  }

  @GET
  @Path("list")
  @Produces(MediaType.APPLICATION_JSON)
  public List<User> getCustomers()
  {
    return customers;
  }

  @GET
  @Path("customer")
  @Produces(MediaType.APPLICATION_JSON)
  public User getCustomer(@QueryParam("id") int id)
  {
    User requiredUser = userDao.getUserData(id);
    return requiredUser;
  }

  public UserDao getUserDao()
  {
    return userDao;
  }

  public void setUserDao(UserDao userDao)
  {
    this.userDao = userDao;
  }
}
