package com.springbootapplication.rest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class UserDao
{
  public User getUserData(int userId)
  {
    User user = new User();
    try
    {
      Class.forName("com.mysql.jdbc.Driver");
      Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/microservice",
        "root", "root");
      Statement stmt = con.createStatement();
      ResultSet rs = stmt.executeQuery("select * from User where userId=" + userId);
      while (rs.next()){
        user.setId(rs.getInt(1));
      user.setName(rs.getString(2) + rs.getString(3));
      user.setAddress(rs.getString(4) + rs.getString(5));
      user.setPhone(rs.getString(6));
    }
      con.close();
    }
    catch (Exception e)
    {
      System.out.println(e);
    }
    return user;
  }
}
