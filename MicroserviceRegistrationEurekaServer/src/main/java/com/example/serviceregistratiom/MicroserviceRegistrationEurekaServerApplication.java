package com.example.serviceregistratiom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
@EnableEurekaServer
@SpringBootApplication
public class MicroserviceRegistrationEurekaServerApplication {

	public static void main(String[] args) {
	  System.out.println("request came on eureka");
		SpringApplication.run(MicroserviceRegistrationEurekaServerApplication.class, args);
	}
}
