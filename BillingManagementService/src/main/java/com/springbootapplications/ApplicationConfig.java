package com.springbootapplications;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;


@Configuration
public class ApplicationConfig
{
  @javax.inject.Named
  static class JerseyConfig extends ResourceConfig
  {
    public JerseyConfig()
    {
      this.packages("com.springbootapplication.rest");
    }
  }
  @Bean
  public RestTemplate restTemplate(){
    RestTemplate restTemplate = new RestTemplate();
    return restTemplate;
  }
}
