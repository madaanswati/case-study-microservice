package com.springbootapplication;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;


@Configuration
public class ApplicationConfig
{
  @javax.inject.Named
  static class JerseyConfig extends ResourceConfig
  {
    public JerseyConfig()
    {
      this.packages("com.springbootapplication.rest");
    }
  }  
}
