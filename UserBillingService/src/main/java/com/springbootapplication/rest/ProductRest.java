package com.springbootapplication.rest;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.collections.bag.SynchronizedSortedBag;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.springbootapplication.queueupropagation.MessageData;
import com.springbootapplication.queueupropagation.RabbitMqConfig;
import javax.ws.rs.PathParam;

@Named
//@Path("/")
public class ProductRest
{
  private static List<Product> products = new ArrayList<Product>();

  static
  {
    Product product1 = new Product();
    product1.setId(1);
    product1.setSku("abcd1");
    product1.setDescription("Product1");
    Product product2 = new Product();
    product2.setId(2);
    product2.setSku("abcd2");
    product2.setDescription("Product2");
    Product product3 = new Product();
    product3.setId(3);
    product3.setSku("abcd3");
    product3.setDescription("Product3");
    Product product4 = new Product();
    product4.setId(4);
    product4.setSku("abcd4");
    product4.setDescription("Product4");
    products.add(product1);
    products.add(product2);
    products.add(product3);
    products.add(product4);
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<Product> getProducts()
  {
    return products;
  }

  /*@GET
  @Path("product")
  @Produces(MediaType.APPLICATION_JSON)
  public Product getProduct(@QueryParam("id") long id)
  {
    Product prod = null;
    for (Product p : products)
    {
      if (p.getId() == id) prod = p;
    }
    return prod;
  }*/

@GET
//@Path("/customerId")
@Path("/customerId")
  //public void getCustomerID(@QueryParam("customerID") long id )
  public void getCustomerID(@PathParam("customerID") long id )
  {
	System.err.println("Inside getCustoomerMethod---------------->");
	  MessageData messageData = new MessageData();
	  System.out.println("id ==="+id);
	  messageData.setCustomerID(100);
	  ApplicationContext ctx = new AnnotationConfigApplicationContext(RabbitMqConfig.class);
      RabbitTemplate rabbitTemplate = ctx.getBean(RabbitTemplate.class);
      System.out.println("Sending data in Queue---->");
      rabbitTemplate.convertAndSend(messageData);
      System.out.println("data pushed in  Queue---->");
  }  
}
