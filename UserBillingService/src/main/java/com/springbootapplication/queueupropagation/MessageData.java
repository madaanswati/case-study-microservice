package com.springbootapplication.queueupropagation;

public class MessageData {
	private long customerID;

	public long getCustomerID() {
		return customerID;
	}

	public void setCustomerID(long customerID) {
		this.customerID = customerID;
	}

}
